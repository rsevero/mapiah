/// Constants and others definitioons that should be generally available.
const thCommentChar = '#';
const thDecimalSeparator = '.';
const thBackslash = '\\';
const thLineBreak = '\n';
const thDoubleQuote = '"';
const thDoubleQuotePair = r'""';

/// This char is from the private-use chars.
// See [https://www.unicode.org/faq/private_use.html]
const thDoubleQuotePairEncoded = '\uE001';
const thWhitespaceChars = ' \t';

const thIndentation = '  ';

const thMaxEncodingLength = 20;
const thMaxFileLineLength = 80;

const thDefaultEncoding = 'UTF-8';

const thNullValueAsString = '!!! property has null value !!!';
